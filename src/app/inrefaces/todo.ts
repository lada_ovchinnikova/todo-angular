export interface todoItem {
  title?: string;
  done?: boolean;
  active?:boolean;
}