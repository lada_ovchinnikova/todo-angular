import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { todoItem } from 'src/app/inrefaces/todo';


@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent {
  @Input() index: number;

  @Input() inputitem: todoItem;

  @Output() onRemove: EventEmitter<number> = new EventEmitter<number>();
  @Output() onEdit: EventEmitter<string> = new EventEmitter<string>();
  
  constructor() { 
    this.inputitem = {}
    this.index = 0
  }

  edit(): void {
    this.onEdit.emit(
      this.inputitem.title
    );
    this.inputitem.active = !this.inputitem.active; 
    console.log(this.inputitem.active)
  }

  remove(): void {
    this.onRemove.emit(
      this.index
    );
  }
  
  isDone(): void {
    
    this.inputitem.done = !this.inputitem.done; 
    console.log(this.inputitem.done)
  }
}
