import { Component, OnInit } from '@angular/core';
import { todoItem } from 'src/app/inrefaces/todo';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent {
  list: todoItem[] = [
    {title: "to do smth", done: false},
    {title: "to do smth2", done: false},
    {title: "to do smth3", done: false}
  ]
  inputText = ""
  addItem (): void {
    const title: string = this.inputText
    const toDo = {title, done: false};
    this.list.push(toDo);
    this.inputText = ""
  }
  remove(indx: number) {
    this.list = this.list.filter(
      (item: todoItem, i: number) => indx !== i
    )
  }
  edit(item: string) {
    this.inputText = item
  }
  upload(): void  {

  }
}
